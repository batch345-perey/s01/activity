package com.zuitt.wdc044.controllers;

import com.zuitt.wdc044.models.Post;
import com.zuitt.wdc044.services.PostServiceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@CrossOrigin
public class PostController {
    @Autowired
    PostServiceImpl postServiceimpl;


    @RequestMapping(value = "/posts", method = RequestMethod.POST)

    public ResponseEntity<?> createPost(@RequestHeader(value = "Authorization") String stringToken, @RequestBody Post post){
        postServiceimpl.createPost(stringToken, post);

        return new ResponseEntity<>("Post created successfully", HttpStatus.CREATED);
    }

    //controller for getting all posts
    @RequestMapping(value = "/posts", method = RequestMethod.GET)

    public ResponseEntity<?> getPosts(){
        return new ResponseEntity<>(postServiceimpl.getPost(), HttpStatus.OK);
    }

    //This route is for editing or updating a post
    @RequestMapping(value = "/posts/{postid}", method = RequestMethod.PUT)
    public ResponseEntity<?> updatePost(@PathVariable Long postid, @RequestHeader(value = "Authorization") String stringToken, @RequestBody Post post){
        return postServiceimpl.updatePost(postid, stringToken, post);

    }

    @RequestMapping(value = "/posts/{postid}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deletePost(@PathVariable Long postid, @RequestHeader(value = "Authorization") String stringToken){
        return postServiceimpl.deletePost(postid, stringToken);
    }

    @RequestMapping(value ="/myposts", method = RequestMethod.GET)
    public ResponseEntity<?> getMyPosts(@RequestHeader(value = "Authorization")String stringToken){
        return new ResponseEntity<>(postServiceimpl.getMyPosts(stringToken), HttpStatus.OK);
    }

}
