package com.zuitt.wdc044.services;

//we need to have access directly to the users table through the User Model
import com.zuitt.wdc044.models.User;

//we need to have access with the UserRepository because we will be needing the methods inside that repository
import com.zuitt.wdc044.repositories.UserRepository;

//to use the methods inside UserRepository, use @Autowired;
import org.springframework.beans.factory.annotation.Autowired;

//This object contains the user details
import org.springframework.security.core.userdetails.UserDetails;


import org.springframework.security.core.userdetails.UserDetailsService;

// the component Annotation informs or declares that our class is a component class
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Component
public class JwtUserDetailsService implements UserDetailsService {
    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(username);
            if(user == null){
                throw new UsernameNotFoundException("User not found with the username: " + username);
            }

            return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), new ArrayList<>());
    }
}
